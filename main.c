#include <msp430.h>
 
// C������� ������� �������� ���������
unsigned int sinwave[13] = { 2048, 3517, 3957, 4095,
                             3910, 2734, 1928, 1142,
                             498, 8, 239, 755, 1476 };
                             
 
int main(void){
    all_init();
    trans_start();
   
    __bis_SR_register(LPM0_bits + GIE);
 
    return 0;
}
 
/*
* ��������� �������������
*/
void all_init(){
    start_init(); // ��������� ������������
    dma_init(); // ������������� ���
    timerB_init(); // ������������� ������� �
    dac_init(); // ������������� ���
    oa_init(); // ������������� ��
    gpio_init(); // ������������ ����� ������ �����/������
}
/*
* ������ ��������
* ��������� �������� ������ ��� ����������� ����������� �����
* DMAEN � �������� DMA0CTL
*/
void trans_start(){
    DMA0CTL |= DMAEN;
    DAC12_0CTL |= DAC12ENC;
}
 
/*
* ������������ �������� ������ ���
*/
void dma_init(){
    DMA0SA = (__SFR_FARPTR) sinwave; // ������� ������ ���������
    DMA0DA = (__SFR_FARPTR) &DAC12_0DAT; // ������� ������ ���������
    DMA0SZ = 13; // ������� ������� �����
    DMACTL0 = 0x0002; // ����� ������������ ������� (������ �)
   
    // ����� ���� ������� (��. ����.)
    // ������� ������������� ������ ��������� (����� ����������������)
    // � ��������� (����� �� ��������)
    DMA0CTL = DMADT_4 + DMASRCINCR_3 + DMADSTINCR_0;
}
 
/*
* ������������ ������� �
*/
void timerB_init(){
    // ����� ������ ����� ������� (����� "�����")
    // ����� ��������� ������������ (ACLK)
    // ����� �������� �������� ������� (/2)
    TBCTL = MC_1 + TBSSEL_2 + ID_1;
    TBCCR0 = 1;
}
 
/*
* ������������ ���
*/
void dac_init(){
    DAC12_0DAT = 0x00; // DAC_0 output 0V
    ADC12CTL0 |= 0x60; // REF2_5V + REFON
    DAC12_0CTL = DAC12IR + DAC12AMP_5 + DAC12CALON + DAC12LSEL_0;
    while (DAC12_0CTL & 0x200); // �������� ���������� ����������
}
 
/*
* ������������ ��
*/
void oa_init(){
    OA1CTL0 = 0x27;
    OA1CTL1 = 0x04;
}
 
/*
* ��������� ������������
*/
void start_init(){
    WDTCTL = WDTPW | WDTHOLD;   // ��������� ����������� �������
    FLL_CTL0 |= XCAP14PF;
 
    // �������� ������������ �������� �������
    do{
        IFG1 &= ~OFIFG;                 // ���������� ���� OSCFault
        for (i = 0x47FF; i > 0; i--);   // ���� ��������� ����� OSCFault
    }
    while ((IFG1 & OFIFG));
}
 
/*
* ������������ ����� ������ �����/������
*/
void gpio_init(){
    P6SEL |= 0xFF, P6DIR |= 0xFF;
}
